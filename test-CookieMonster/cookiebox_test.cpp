#include "../CookieMonster/cookiebox.h"
#include "catch.h"

TEST_CASE( "Cookiebox: grow(), getAmountOfCookies() &  setAmountOfCookies()") {
    // cookie box parameters
    double carryingCapacity = 1.0;
    double growthRate = 0.05;
    double intialAmountOfRessource = 1.0;

    CookieBox box(growthRate, carryingCapacity, intialAmountOfRessource);
    REQUIRE(box.getAmountOfCookies() == 1.0);

    box.grow();
    REQUIRE(box.getAmountOfCookies() == 1.0);

    box.setAmountOfCookies(0.5);
    box.grow();
    REQUIRE(box.getAmountOfCookies() == 0.5125);

    box.setAmountOfCookies(-5); // negative amount of cookies makes no sense
    REQUIRE(box.getAmountOfCookies() >= 0.0); // Test fails
}
