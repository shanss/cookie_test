TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cookiebox_test.cpp \
    ../CookieMonster/cookiebox.cpp

HEADERS += \
    catch.h \
    ../CookieMonster/cookiebox.h
