#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.h"


int add(int a, int b)
{
    return a + b;
}


bool test_add(int a, int b)
{
    int value = add(2, 3);

    if (value != 5) {
        return false;
    }

    value = add(5, -3);

    if (value != 2) {
        return false;
    }

    return true;
}
