#include "monster.h"
#include "parameters.h"


/**********************************************
 * The constructor used to initialize the
 * Monster instance.
 **********************************************/
Monster::Monster(int maximumStayTime,
                 double effectiveness,
                 double energyLevel,
                 double movementCosts,
                 double timeCosts,
                 CookieBox *currentBox,
                 Landscape *landscape)
{
  this->stayTime = maximumStayTime;
  this->effectiveness = effectiveness;
  this->movementCosts = movementCosts;
  this->energyLevel = energyLevel;
  this->timeCosts = timeCosts;
  this->currentBox = currentBox;
  this->landscape = landscape;
  this->intakeRate = 0;
  this->time = 0;
}

/**********************************************
 * function to calculate the monsters uptake of
 * cookies.
 **********************************************/
void Monster::feed()
{
  // if this is a new box, reset the intake rate
  if (time == 0)
      intakeRate = 0;

  // there is a certain amount of resource available
  double availableCookies = currentBox->getAmountOfCookies();

  // the monster feeds with a certain effectiveness
  double uptake = effectiveness * availableCookies;

  // and the monster is getting more energy
  energyLevel += uptake;
  intakeRate = uptake;

  // but the cookie box is getting emptyier
  double reducedCookies = availableCookies - uptake;
  currentBox->setAmountOfCookies(reducedCookies);


  // apply costs for just beeing
  // basal metabolism
  energyLevel -= timeCosts;
  intakeRate  -= timeCosts;
  time++;
}

/**********************************************
 * This function lets the monster move to the next
 * cookie box (which might be the old one as
 * well), if the maximum time the
 * monster stays in one cookie box is exceeded.
 * (We assume that also staying in the same cookie
 * box imposes costs, because the monster has to
 * move after its maximum stay time. It simply
 * can't sit still). Moves imposes costs.
 **********************************************/
void Monster::move(unsigned nextBox)
{
  // check if the next box is part of the landscape
  // if not so, the next box is turned into a part
  // of the landscape by applying the modulo operation
  if (!(nextBox < landscape->getNumberOfBoxes()))
    nextBox = nextBox % landscape->getNumberOfBoxes();

  // check if the stay time is exceeded
  if (time >= stayTime){
    // apply costs for movement
    energyLevel -= movementCosts;
    intakeRate  -= movementCosts;
    // has the monster still energy to move?
    if (energyLevel > 0) { // monster moves
      currentBox = landscape->getCookieBox(nextBox);
    }
    else { // monster is dead
      energyLevel = 0;
      intakeRate = 0;
    }
    // reset time counter and intake rate
    time = 0;
  }
}

/**********************************************
 * This function returns the private member variable
 * to functions outside the class Monster.
 **********************************************/
double Monster::getIntakeRate()
{
  return intakeRate;
}
