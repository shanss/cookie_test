#ifndef COOKIEBOX_H
#define COOKIEBOX_H

/**********************************************
 * This class represents cookie boxes.
 **********************************************/
class CookieBox
{
public: // public members
  CookieBox();
  CookieBox(double growthRate,
            double carryingCapacity,
            double amountOfCookies);
  void grow();
  double getAmountOfCookies();
  void setAmountOfCookies(double value);

private: // private members
  double carryingCapacity;
  double growthRate;
  double amountOfCookies;
};

#endif // COOKIEBOX_H
