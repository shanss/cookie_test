#ifndef MONSTER_H
#define MONSTER_H

#include "cookiebox.h"
#include "landscape.h"

/**********************************************
 * This class represents the cookie monster.
 * It has some private status variables and
 * public functions to operate (basically feed
 * and move).
 **********************************************/
class Monster
{
public:
  // This constructor has initial values for
  // its parameters. They are overwritten as
  // soon as the constructor is called with
  // other parameters.
  Monster(int stayTime = 0,
          double effectiveness = 0,
          double energyLevel = 0,
          double movementCosts = 0,
          double timeCosts = 0,
          CookieBox *currentBox = nullptr,
          Landscape *landscape = nullptr);
  void feed();
  void move(unsigned nextBox);
  double getIntakeRate();

private:
  int stayTime;
  int time;
  double effectiveness;
  double energyLevel;
  double intakeRate;
  double movementCosts;
  double timeCosts;
  CookieBox *currentBox;
  Landscape *landscape;
};

#endif // MONSTER_H
