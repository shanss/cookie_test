#include <chrono>
#include "simulation.h"
#include "parameters.h"

/**********************************************
 * The constructor used to initialize the
 * Simulation instance.
 **********************************************/
Simulation::Simulation()
{
  // declare a Parameters object to access
  // the parameters needed for initializations
  Parameters p;

  // initializing the random number generator
  auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
  mt = std::mt19937(seed);
  nextBox = std::uniform_int_distribution<int>(0, p.numberOfBoxes - 1);

  // create the landscape
  for (int i = 0; i < p.numberOfBoxes; i++) {
    // call CookieBox constructor
    CookieBox box(p.growthRate,
                  p.carryingCapacity,
                  p.intialAmountOfRessource);
    landscape.addCookieBox(box);
  }

  // create the Monster
  // call Monster constructor
  monster = Monster(p.stayTime,
                    p.effectiveness,
                    p.initialEnergyLevel,
                    p.movementCosts,
                    p.timeCosts,
                    landscape.getCookieBox(p.initialBox),
                    &landscape);

  // set the time horizon of the simulation
  timeHorizon = p.timeHorizon;
}

/**********************************************
 * This function performs the simulation run.
 * It calls all the sub functions to perform
 * the simulation dynamics. It gets the addresses
 * of the vectors needed for the output as parameter.
 **********************************************/
void Simulation::run(std::vector<double> &monsterIntakeRateVector,
                     std::vector<std::vector<double> > &cookieboxVector)
{
  // iterate over all time steps
  for(int time = 0; time < timeHorizon; time++){
    // iterate over all boxes
    // and let them grow
    for(unsigned i = 0; i < landscape.getNumberOfBoxes(); i++) {
      CookieBox* box = landscape.getCookieBox(i);
      box->grow();
    }
    // let the monster feed and move
    monster.feed();
    monster.move(nextBox(mt));

    // update the vectors for plotting
    monsterIntakeRateVector.push_back(monster.getIntakeRate());
    for (unsigned i = 0; i < landscape.getNumberOfBoxes(); i++) {
      CookieBox* box = landscape.getCookieBox(i);
      cookieboxVector[i].push_back(box->getAmountOfCookies());
    }
  }
}
