#-------------------------------------------------
#
# Project created by QtCreator 2015-09-08T16:55:14
#
#-------------------------------------------------

QT       += core gui charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = CookieMonsterApp
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    simulation.cpp \
    monster.cpp \
    cookiebox.cpp \
    landscape.cpp \
    parameters.cpp

HEADERS  += mainwindow.h \
    simulation.h \
    monster.h \
    cookiebox.h \
    landscape.h \
    parameters.h

FORMS    += mainwindow.ui
