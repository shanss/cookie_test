#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "simulation.h"

/*****************************************
 * The constructor of the MainWindow class.
 *****************************************/
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  // Setting up the charts for plotting
  cookieboxEnergyChart = new QChart();
  ui->cookieboxEnergy->setChart(cookieboxEnergyChart);
  ui->cookieboxEnergy->setRenderHint(QPainter::Antialiasing); // looks prettier

  monsterIntakeChart = new QChart();
  ui->monsterIntakeRate->setChart(monsterIntakeChart);
  ui->monsterIntakeRate->setRenderHint(QPainter::Antialiasing);
}

/*****************************************
 * The destructor of the MainWindow class.
 *****************************************/
MainWindow::~MainWindow()
{
  // deleting the user interface and the chart
  delete ui;
  delete cookieboxEnergyChart;
  delete monsterIntakeChart;
}

/*****************************************
 * When the Go button is clicked, this
 * function is executed.
 *****************************************/
void MainWindow::on_goButton_clicked()
{
  clearCharts();
  Simulation sim;
  sim.run(monsterIntakeVector, coockieboxVector);
  writeOutput();
  drawCharts();
}

/*****************************************
 * This function clears the vectors needed
 * for output and the graphs, which are
 * going to be plotted
 *****************************************/
void MainWindow::clearCharts()
{
  //clear all output vectors
  monsterIntakeVector.clear();
  coockieboxVector.resize(parameters.numberOfBoxes);
  for (int i = 0; i < parameters.numberOfBoxes; i++) {
    coockieboxVector[i].clear();
  }

  // clear monster chart
  monsterIntakeChart->removeAllSeries(); // also calls delete

  // clear coockie chart
  cookieboxEnergyChart->removeAllSeries(); // calls delete for all series
}

/**********************************************
 * This function writes the data generated in
 * each time step and stored in the output
 * vectors to the output file.
 **********************************************/
void MainWindow::writeOutput()
{
  // constructing a new QFile object
  QFile outputFile("OutputFile.csv");
  outputFile.open(QFile::WriteOnly | QFile::Truncate | QFile::Text);

  // binding the text stream to the output file
  QTextStream out;
  out.setDevice(&outputFile);
  out.setRealNumberPrecision(2);

  // header line in the output file
  out << "time, "
         "intake rate, "
         "cookie box sizes" << endl;

  // iterate over the timeVector
  for (int time = 0; time < parameters.timeHorizon; time++) {
    // write the time to the file
    out << time << ", ";
    // write the intake rate of the monster
    // at time t to the file
    out << monsterIntakeVector.at(time) << ", ";
    // iterate over the resource vectors
    // (there is one for each cookie box)
    for (auto b : coockieboxVector) {
      // b represents one box,
      // the amount of the resource
      // at time t is written to the file
      out << b.at(time) << ", ";
    }
    // line break after one line for
    // time t is written
    out << "\n";
  }

  // closing the output file
  outputFile.close();
}

/**********************************************
 * This function is responsible for plotting the
 * tracked dynamics to the user interface.
 **********************************************/
void MainWindow::drawCharts()
{
  // Monster Chart
  QLineSeries *monsterIntakeSeries = new QLineSeries();
  monsterIntakeSeries->setColor(Qt::black); // default color: blue
  monsterIntakeSeries->setName("Cookie monster intake rate");

  for (int time = 0; time < parameters.timeHorizon; time++) {
    monsterIntakeSeries->append(time, monsterIntakeVector.at(time));
  }
  monsterIntakeChart->addSeries(monsterIntakeSeries);

  monsterIntakeChart->createDefaultAxes();
  monsterIntakeChart->axisX()->setTitleText("Time [steps]");
  monsterIntakeChart->axisY()->setTitleText("Energy");

  // Coockie box chart
  for (int nBox = 0; nBox < parameters.numberOfBoxes; nBox++) {
    QLineSeries *cookieboxSeries = new QLineSeries();
    QString cookieboxName = QString::number(nBox) + " Cookiebox";
    cookieboxSeries->setName(cookieboxName);

    for (int time = 0; time < parameters.timeHorizon; time++) {
      cookieboxSeries->append(time, coockieboxVector[nBox][time]);
    }
    cookieboxEnergyChart->addSeries(cookieboxSeries);
  }

  cookieboxEnergyChart->createDefaultAxes();
  cookieboxEnergyChart->axisX()->setTitleText("Time [steps]");
  cookieboxEnergyChart->axisY()->setTitleText("Energy");
}
