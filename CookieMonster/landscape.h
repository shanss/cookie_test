#ifndef LANDSCAPE_H
#define LANDSCAPE_H

#include "cookiebox.h"
#include<vector>

/**********************************************
 * This class represents the landscape, where
 * the cookie monster forages. It basicly
 * consists of a vector containing all cookie
 * boxes plus some simple logic.
 **********************************************/
class Landscape
{
public:
  CookieBox *getCookieBox(int index);
  void addCookieBox(CookieBox box);
  unsigned getNumberOfBoxes();

private:
  std::vector<CookieBox> cookieBoxes;
};

#endif // LANDSCAPE_H
