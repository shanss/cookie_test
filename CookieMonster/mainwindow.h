#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextStream>
#include <QFile>
#include <QtCharts>
#include "parameters.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void on_goButton_clicked();

private:
  Parameters parameters;

  // Vectors needed to store simulation output
  std::vector<double> monsterIntakeVector;
  std::vector<std::vector<double> > coockieboxVector;

  // needed for plotting
  QChart *cookieboxEnergyChart;
  QChart *monsterIntakeChart;

  // some custom functions
  void drawCharts();
  void clearCharts();
  void writeOutput();

  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
