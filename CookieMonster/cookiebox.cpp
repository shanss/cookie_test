#include "cookiebox.h"

/**********************************************
 * The default constructor. It has to be defined
 * explicitly, because there is another constructor
 * defined and we need the default constructor
 * to initialize the landscape vector.
 **********************************************/
CookieBox::CookieBox()
{
}

/**********************************************
 * The constructor used to initialize the
 * CookieBox instances.
 **********************************************/
CookieBox::CookieBox(double growthRate,
                     double carryingCapacity,
                     double amountOfCookies)
{
  this->growthRate = growthRate;
  this->carryingCapacity = carryingCapacity;
  this->amountOfCookies = amountOfCookies;
}

/**********************************************
 * This function calculates the growth of a cookie
 * box. It gets a pointer at the cookie box in
 * question as a parameter.
 **********************************************/
void CookieBox::grow()
{
  double N = amountOfCookies;
  double r = growthRate;
  double K = carryingCapacity;

  // the logistic equation
  N = N + r * N * (1 - N/K);

  amountOfCookies = N;
}

/**********************************************
 * This function enables functions outside the
 * class CookieBoxthe to set the private member
 * variable amountOfResource.
 **********************************************/
void CookieBox::setAmountOfCookies(double value)
{
  if (value < 0) value  = 0.0;
  
  amountOfCookies = value;
}

/**********************************************
 * This function returns the private member variable
 * to functions outside the class CookieBox.
 **********************************************/
double CookieBox::getAmountOfCookies()
{
  return amountOfCookies;
}
