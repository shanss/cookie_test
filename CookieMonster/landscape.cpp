#include <iostream>
#include <cmath> //for std::abs()
#include "landscape.h"

/**********************************************
 * This functions adds one cookie box to the
 * landscape. It get a CookieBox object as
 * parameter and appends it to a vector
 * containing all cookie boxes of the landscape
 **********************************************/
void Landscape::addCookieBox(CookieBox box)
{
  cookieBoxes.push_back(box);
}

/**********************************************
 * This function returns the private member to
 * calling functions outside the Landscape class.
 **********************************************/
unsigned Landscape::getNumberOfBoxes()
{
    return cookieBoxes.size();
}

/**********************************************
 * This functions returns a cookie box. Before
 * doing so it checks if the index, which it
 * gets as parameter is less than the number
 * of cookie boxes. The function returns a
 * pointer to the cookie box with the
 * corresponding index.
 **********************************************/
CookieBox* Landscape::getCookieBox(int index)
{
  if (index >= 0 && index < cookieBoxes.size()) {
    return &cookieBoxes[index];
  } else { // write an error message to the console
    std::cout << "Landscape::getCookieBox():"
                 "Index out of range!\n"
                 "Index % numberOfBoxes"
                 "is chosen instead.";
    // instead if crashing or terminating the
    // program the index is converted into a
    // usable number by performing the modulo
    // operation.
    return &cookieBoxes[std::abs(index) % cookieBoxes.size()];
  }
}
