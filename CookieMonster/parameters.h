#ifndef PARAMETERS_H
#define PARAMETERS_H

class Parameters
{
public:
  // simulation parameters
  int timeHorizon = 500;
  int numberOfBoxes = 5;

  // monster parameters
  int initialBox = 0;
  int stayTime = 15;
  double movementCosts = 0.2;
  double timeCosts = 0.01;
  double effectiveness = 0.1;
  double initialEnergyLevel = 0.5;

  // cookie box parameters
  double carryingCapacity = 1.0;
  double growthRate = 0.05;
  double intialAmountOfRessource = 1.0;
};

#endif // PARAMETERS_H
