#ifndef SIMULATION_H
#define SIMULATION_H

#include <random>
#include "landscape.h"
#include "monster.h"

class Simulation
{
public:
  Simulation();
  void run(std::vector<double> &monsterIntakeRateVector,
           std::vector<std::vector<double> > &cookieboxVector);

private:
  // random number generator
  std::uniform_int_distribution<int> nextBox;
  std::mt19937 mt;

  int timeHorizon;
  Landscape landscape;
  Monster monster;
};

#endif // SIMULATION_H
